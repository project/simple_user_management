<?php

/**
 * @file
 * Documentation related to simple_user_management.
 */

/**
 * Allow bypassing the role delegation for deletion.
 *
 * For example, if your site has roles that are synced from an external tool
 * and therefore not manageable by the current user, but you still want
 * them to be able to delete the user.
 *
 * @param bool $bypass_role_delegation_check
 *   Whether bypass role delegation check should be allowed. Default false.
 * @param array $context
 *   Keyed by 'current_user' (the logged in user) and 'target_user' (the user
 *   to whom the deletion action is about to take place.
 */
function hook_simple_user_management_delete_role_delegation_check_alter(
  bool $bypass_role_delegation_check,
  array &$context,
) {
  return $bypass_role_delegation_check;
}
