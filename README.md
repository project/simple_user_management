# Simple User Management

This module provides a way to let clients easily approve users as well as grant
them particular roles such as site editors assigning editor role to other new
users (but avoiding editors granting or gaining administrator access). The
module provides quick access operations from the people list to approve and
delegate roles.

It uses the excellent Role Delegation module under the hood for delegating
roles and provides it's own simple interface for approving users without
the need for the 'administer users' permission.

It also provides a simple interface to allow deactivation or cancellation of 
users (optional, via two separate permissions). It only allows deactivation 
or cancellation of users that have only roles that the logged in user can 
delegate. Eg, if a user has the administrator role and the logged in user is
an editor with the ability to delegate the editor role only, the logged in 
user will not be able to deactivate the administrator.

## Example use cases

You want to provide a very simple interface to let your client manage site
editors and authors but want to avoid your clients gaining administrator access.
You may have opened up registration with approval and want your client to be
able to approve users (eg, a new colleague) and grant them a particular role.

## Requirements

This module requires the following module:

- [Role Delegation](https://www.drupal.org/project/role_delegation)

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

- Place it in your modules folder and enable it on the modules page.
- Grant the desired role (such as 'editor') 'view user information' permission
  via `admin > people > permissions`.
- Grant the desired role (such as 'editor') the desired role delegation
  permissions such as delegating the 'editor' and 'author' role, avoiding
  allowing delegation of the administrator role to keep your site safe.
- Optionally grant the 'create user accounts' permission (eg, to 'editor')
  (otherwise users must register via '/user/register' and then the editor
  can activate and grant the role).
- Go to `admin > structure > views > 'People'`
  (/admin/structure/views/view/user_admin_people) and set the permission of
  the view to be 'view user information' and save the view.
- Optionally add a quicklink or add the `admin > people` to a menu location
  that your non-admin role (such as 'editor') can access

## Feedback on this module

Please add issues with feature requests as well as feedback on the existing
functionality.

## Supporting organizations

Maintenance of this module is sponsored by [Soapbox](https://www.drupal.org/soapbox-communications-ltd)
Development this module was sponsored by [Fat Beehive](https://www.drupal.org/fat-beehive)
