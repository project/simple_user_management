<?php

namespace Drupal\Tests\simple_user_management\Traits;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Helper trait used by tests.
 */
trait SimpleUserManagementTestHelperTrait {

  use StringTranslationTrait;
  use UserCreationTrait;

  /**
   * The user manager.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $userManager;

  /**
   * The author.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $author;

  /**
   * The admin.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $admin;

  /**
   * The user manager role.
   *
   * @var string
   */
  protected $userManagerRole;

  /**
   * The author role.
   *
   * @var string
   */
  protected $authorRole;

  /**
   * Set up the admin people view as per module homepage instructions.
   */
  protected function setUpAdminPeopleView(): void {
    $config = \Drupal::configFactory()->getEditable('views.view.user_admin_people');
    $config->set('display.default.display_options.access.options.perm', 'access user profiles');
    $config->save();
    drupal_flush_all_caches();
  }

  /**
   * Set up default user with access to all features of this module.
   */
  protected function setUpDefaultUserWithAccess(): void {
    $user_manager_permissions = [
      'access administration pages',
      'view the administration theme',
      'access user profiles',
      'approve user accounts',
      'create user accounts',
      'deactivate user accounts',
      'delete user accounts',
      'change user passwords',
    ];
    $author_permissions = [
      'access administration pages',
      'view the administration theme',
    ];

    // Create 3 roles to test out.
    $this->userManagerRole = $this->drupalCreateRole($user_manager_permissions, 'user_manager_role');
    $this->authorRole = $this->drupalCreateRole($author_permissions, 'author_role');

    // Create 3 users with those roles.
    // User manager needs random role removed and switched to named role.
    $this->userManager = $this->drupalCreateUser($user_manager_permissions, 'user_manager_test');
    $roles = $this->userManager->getRoles();
    $this->userManager->removeRole(end($roles));
    $this->userManager->addRole($this->userManagerRole);
    $this->userManager->save();

    // Author needs random role removed and switched to named role.
    $this->author = $this->drupalCreateUser($author_permissions, 'author_test');
    $roles = $this->author->getRoles();
    $this->author->removeRole(end($roles));
    $this->author->addRole($this->authorRole);
    $this->author->save();

    // Admin user.
    $this->admin = $this->drupalCreateUser([], 'admin_test', TRUE);

    // Login as the user manager.
    $this->drupalLogin($this->userManager);
  }

}
