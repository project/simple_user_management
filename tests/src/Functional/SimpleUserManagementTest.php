<?php

namespace Drupal\Tests\simple_user_management\Functional;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\simple_user_management\Traits\SimpleUserManagementTestHelperTrait;
use Drupal\user\Entity\Role;
use Drupal\user\Entity\User;

/**
 * Contains Simple User Management functionality tests.
 *
 * @group simple_user_management
 */
class SimpleUserManagementTest extends BrowserTestBase {

  use StringTranslationTrait;
  use SimpleUserManagementTestHelperTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'simple_user_management',
    'role_delegation',
    'user',
    'views',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->setUpAdminPeopleView();
    $this->setUpDefaultUserWithAccess();
  }

  /**
   * Tests activating a blocked user.
   */
  public function testActivate() {
    $this->drupalGet('admin/people');
    $this->assertSession()->statusCodeEquals(200);

    // Test activation.
    $this->author->block();
    $this->author->save();
    $this->assertFalse($this->author->isActive(), 'Author is blocked');
    $this->drupalGet('admin/manage-users/approve/' . $this->author->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->click('#edit-approve');

    // Reload user.
    $this->author = User::load($this->author->id());
    $this->assertTrue($this->author->isActive(), 'Author is active');

    // Check activation of already active user.
    $this->drupalGet('admin/manage-users/approve/' . $this->author->id());
    $this->assertSession()->pageTextContains('The user is already active');
  }

  /**
   * Tests changing a password of a user.
   */
  public function testChangePassword() {
    $this->drupalGet('admin/people');
    $this->assertSession()->statusCodeEquals(200);

    // Test password change denied.
    $this->drupalGet('admin/manage-users/change-password/' . $this->author->id());
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet('admin/manage-users/change-password/' . $this->admin->id());
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet('admin/manage-users/change-password/' . $this->userManager->id());
    $this->assertSession()->statusCodeEquals(403);

    // Grant permission to assign the role.
    /** @var \Drupal\user\RoleInterface $role */
    $role = Role::load($this->userManagerRole);
    $role->grantPermission('assign ' . $this->authorRole . ' role');
    $role->save();

    // Attempt to change password.
    $this->drupalGet('admin/manage-users/change-password/' . $this->author->id());
    $this->submitForm([
      'password[pass1]' => 'test-weak-password',
      'password[pass2]' => 'test-weak-password',
    ], 'Change password');

    // Reset permission.
    $role->revokePermission('assign ' . $this->authorRole . ' role');
    $role->save();
  }

  /**
   * Tests activating a blocked user.
   */
  public function testAssignRole() {
    $this->drupalGet('admin/people');
    $this->assertSession()->statusCodeEquals(200);

    // Deny edit and roles page by default.
    $this->drupalGet('user/' . $this->author->id() . '/edit');
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet('user/' . $this->author->id() . '/roles');
    $this->assertSession()->statusCodeEquals(403);

    // Grant assigning permission.
    $role_object = Role::load($this->userManagerRole);
    $role_object->grantPermission('assign ' . $this->authorRole . ' role');
    $role_object->save();
    $this->drupalGet('user/' . $this->author->id() . '/roles');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementExists('css', '#edit-role-change-' . str_replace('_', '-', $this->authorRole));
    $this->assertSession()->elementNotExists('css', '#edit-role-change-' . str_replace('_', '-', $this->userManagerRole));
    $this->assertSession()->elementNotExists('css', '#edit-role-change-admin');

    // Change roles of author.
    $this->submitForm([
      'role_change[' . $this->authorRole . ']' => 0,
    ], 'Save');
    $this->author = User::load($this->author->id());
    $this->assertFalse($this->author->hasRole($this->authorRole), 'Author no longer has author role');
  }

}
