<?php

namespace Drupal\Tests\simple_user_management\FunctionalJavascript;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\Tests\simple_user_management\Traits\SimpleUserManagementTestHelperTrait;
use Drupal\user\Entity\Role;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;

/**
 * Contains Simple User Management functionality tests.
 *
 * @group simple_user_management
 */
class SimpleUserManagementJavascriptTest extends WebDriverTestBase {

  use StringTranslationTrait;
  use SimpleUserManagementTestHelperTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'simple_user_management',
    'role_delegation',
    'user',
    'views',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->setUpAdminPeopleView();
    $this->setUpDefaultUserWithAccess();
  }

  /**
   * Tests blocking an active user.
   */
  public function testBlock() {
    $this->drupalGet('admin/people');

    // Test de-activating.
    $this->assertTrue($this->author->isActive(), 'Author is active');
    $this->drupalGet('admin/manage-users/deactivate/' . $this->author->id());

    // Role permission not yet available, not allowed to de-activate.
    $this->assertSession()->pageTextContains('You are not allowed to deactivate this user.');

    // Grant permission and try again.
    $role_object = Role::load($this->userManagerRole);
    $role_object->grantPermission('assign ' . $this->authorRole . ' role');
    $role_object->save();
    $this->drupalGet('admin/manage-users/deactivate/' . $this->author->id());
    $this->click('#edit-deactivate');

    // Reload user.
    $this->assertSession()->waitForElementVisible('css', '#edit-user');
    $this->author = User::load($this->author->id());
    $this->assertTrue($this->author->isBlocked(), 'Author is blocked');

    // Check de-activation of already active user.
    $this->drupalGet('admin/manage-users/deactivate/' . $this->author->id());
    $this->assertSession()->pageTextContains('This user is already blocked');

    // Check de-activation of self.
    $this->drupalGet('admin/manage-users/deactivate/' . $this->userManager->id());
    $this->assertSession()->pageTextContains('You are not allowed to deactivate yourself');

    // Check de-activation of admin.
    $this->drupalGet('admin/manage-users/deactivate/' . $this->admin->id());
    $this->assertSession()->pageTextContains('You are not allowed to deactivate this user');
  }

  /**
   * Tests blocking an active user.
   */
  public function testDelete() {
    $this->drupalGet('admin/people');

    // Test deleting.
    $this->assertTrue($this->author->isActive(), 'Author is active');
    $this->drupalGet('admin/manage-users/delete/' . $this->author->id());

    // Role permission not yet available, not allowed to de-activate.
    $this->assertSession()->pageTextContains('You are not allowed to delete this user.');

    // Grant permission and try again.
    $role_object = Role::load($this->userManagerRole);
    $role_object->grantPermission('assign ' . $this->authorRole . ' role');
    $role_object->save();
    $this->drupalGet('admin/manage-users/delete/' . $this->author->id());
    $this->submitForm([
      'user_cancel_method' => 'user_cancel_reassign',
    ], 'Delete');

    // Reload user.
    $this->assertSession()->waitForElementVisible('css', '#edit-user');
    $this->author = User::load($this->author->id());
    $this->assertFalse($this->author instanceof UserInterface, 'Author is deleted');

    // Check de-activation of self.
    $this->drupalGet('admin/manage-users/delete/' . $this->userManager->id());
    $this->assertSession()->pageTextContains('You are not allowed to delete yourself');

    // Check de-activation of admin.
    $this->drupalGet('admin/manage-users/delete/' . $this->admin->id());
    $this->assertSession()->pageTextContains('You are not allowed to delete this user');
  }

}
