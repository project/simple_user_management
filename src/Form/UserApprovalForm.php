<?php

namespace Drupal\simple_user_management\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A form to approve an inactive user.
 *
 * @package Drupal\simple_user_management\Form
 */
class UserApprovalForm extends FormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Variable for messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a UserApprovalForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, MessengerInterface $messenger) {
    $this->entityTypeManager = $entity_type_manager;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'user_approval_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $user = FALSE) {
    $uid = $user;
    /** @var \Drupal\user\UserInterface $user */
    $user = $this->entityTypeManager->getStorage('user')->load($uid);
    if ($user instanceof UserInterface && !$user->isActive()) {
      $form['intro'] = [
        '#markup' => '<p>' . $this->t('Are you sure you want to approve the following user and activate their account?') . '</p>',
      ];

      $form['user'] = [
        '#theme' => 'item_list',
        '#items' => [],
      ];
      $form['user']['#items'][] = [
        '#markup' => $this->t('Username:') . ' ' . $user->getDisplayName(),
      ];
      $form['user']['#items'][] = [
        '#markup' => $this->t('Email:') . ' ' . $user->getEmail(),
      ];

      $form['actions'] = [
        '#type' => 'container',
      ];

      $form['actions']['approve'] = [
        '#type' => 'submit',
        '#value' => $this->t('Approve'),
        '#attributes' => [
          'class' => [
            'button',
            'button--primary',
          ],
        ],
      ];

      $form['uid'] = [
        '#type' => 'hidden',
        '#value' => $uid,
      ];
    }
    elseif ($user instanceof UserInterface && $user->isActive()) {
      $message = $this->t('The user is already active');
      $this->messenger->addMessage($message, 'warning');
    }
    else {
      $message = $this->t('Unable to load the user details.');
      $this->messenger->addMessage($message, 'warning');
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Activate the user.
    $user = $this->entityTypeManager->getStorage('user')->load($form_state->getValue('uid'));
    $user->activate();
    $user->save();

    $form_state->setRedirectUrl(Url::fromUserInput('/admin/people'));

    // Let the activator know the user has been activated.
    $message = $this->t('The user has been successfully activated.');
    $this->messenger->addMessage($message, 'status');
  }

}
