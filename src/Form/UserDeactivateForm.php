<?php

namespace Drupal\simple_user_management\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Drupal\role_delegation\DelegatableRolesInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A form to deactivate an active user.
 *
 * @package Drupal\simple_user_management\Form
 */
class UserDeactivateForm extends FormBase {

  use MessengerTrait;

  /**
   * Constructs a UserDeactivateForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user.
   * @param \Drupal\role_delegation\DelegatableRolesInterface $delegatableRoles
   *   The delegatable roles service.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
    protected AccountProxyInterface $currentUser,
    protected DelegatableRolesInterface $delegatableRoles,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('current_user'),
      $container->get('delegatable_roles')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'user_approval_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $user = FALSE) {
    $uid = $user;
    /** @var \Drupal\user\UserInterface $user */
    if ($user = $this->entityTypeManager->getStorage('user')->load($uid)) {
      $current_user = $this->currentUser;

      // No deactivation of self.
      if ($user->id() === $current_user->id()) {
        $message = $this->t('You are not allowed to deactivate yourself.');
        $this->messenger()->addMessage($message, 'warning');
        return $form;
      }

      // No deactivation of already blocked user.
      if ($user->isBlocked()) {
        $message = $this->t('This user is already blocked.');
        $this->messenger()->addMessage($message, 'warning');
        return $form;
      }

      // Only allow deactivation if the logged in user has permission to
      // delegate all roles that the user being checked has.
      $allow_deactivation = TRUE;
      $assignable_roles = $this->delegatableRoles->getAssignableRoles($current_user);
      $user_roles = $user->getRoles();
      foreach ($user_roles as $user_role) {
        if ($user_role != 'authenticated' && !in_array($user_role, array_keys($assignable_roles))) {
          $allow_deactivation = FALSE;
        }
      }
      var_dump('$assignable_roles');
      var_dump($assignable_roles);
      var_dump('$user_roles');
      var_dump($user_roles);

      if ($allow_deactivation) {

        // Hidden fields to match expectations of user_cancel().
        $form['user_cancel_method'] = [
          '#type' => 'hidden',
          '#value' => 'user_cancel_block',
        ];
        $form['user_cancel_confirm'] = [
          '#type' => 'hidden',
          '#value' => '',
        ];
        $form['user_cancel_notify'] = [
          '#type' => 'hidden',
          '#value' => '',
        ];
        $form['uid'] = [
          '#type' => 'hidden',
          '#value' => $user->id(),
        ];
        $form['access'] = [
          '#type' => 'hidden',
          '#value' => TRUE,
        ];

        // Informational fields for the logged in user.
        $form['intro'] = [
          '#markup' => '<p>' . $this->t('Are you sure you want to deactivate the following user? This will disable the account but keep its contents.') . '</p>',
        ];
        $form['user'] = [
          '#theme' => 'item_list',
          '#items' => [],
        ];
        $form['user']['#items'][] = [
          '#markup' => $this->t('Username:') . ' ' . $user->getDisplayName(),
        ];
        $form['user']['#items'][] = [
          '#markup' => $this->t('Email:') . ' ' . $user->getEmail(),
        ];
        $form['actions'] = [
          '#type' => 'container',
        ];
        $form['actions']['deactivate'] = [
          '#type' => 'submit',
          '#value' => $this->t('Deactivate'),
          '#attributes' => [
            'class' => [
              'button',
              'button--primary',
            ],
          ],
        ];
        $form['uid'] = [
          '#type' => 'hidden',
          '#value' => $uid,
        ];
      }
      else {
        $message = $this->t('You are not allowed to deactivate this user.');
        $this->messenger()->addMessage($message, 'warning');
      }
    }
    else {
      $message = $this->t('Unable to load the user details.');
      $this->messenger()->addMessage($message, 'warning');
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    user_cancel(
      $form_state->getValues(),
      $form_state->getValue('uid'),
      $form_state->getValue('user_cancel_method')
    );

    $form_state->setRedirectUrl(Url::fromUserInput('/admin/people'));

    // Let the deactivator know the user has been deactivated.
    $message = $this->t('The user has been successfully deactivated.');
    $this->messenger()->addMessage($message, 'status');
  }

}
