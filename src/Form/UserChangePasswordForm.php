<?php

namespace Drupal\simple_user_management\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\role_delegation\DelegatableRolesInterface;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * A form to change a user's password.
 *
 * @package Drupal\simple_user_management\Form
 */
class UserChangePasswordForm extends FormBase {

  /**
   * Constructs a UserApprovalForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messengerService
   *   The messenger service.
   * @param \Drupal\role_delegation\DelegatableRolesInterface $delegatableRoles
   *   The delegate-able roles service.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
    protected MessengerInterface $messengerService,
    protected DelegatableRolesInterface $delegatableRoles,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('messenger'),
      $container->get('delegatable_roles'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'user_change_password_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $user = FALSE) {
    $uid = $user;
    /** @var \Drupal\user\UserInterface $user */
    $user = $this->entityTypeManager->getStorage('user')->load($uid);

    if ($user instanceof UserInterface) {
      $user_roles = $user->getRoles();
      $roles = $this->delegatableRoles->getAssignableRoles($this->currentUser());
      $assignable_roles = array_keys($roles);
      $assignable_roles[] = AccountInterface::AUTHENTICATED_ROLE;
      if (
        empty($roles)
        || array_diff($user_roles, $assignable_roles + [AccountInterface::AUTHENTICATED_ROLE])
      ) {
        $this->messengerService->addError($this->t('You do not have permission to change the password of this user.'));
        throw new AccessDeniedHttpException();
      }

      $form['intro'] = [
        '#markup' => '<p>' . $this->t('Manually change the password of this user (in most cases you should encourage the user should reset their password instead is this route means that you know their password).') . '</p>',
      ];

      $form['password'] = [
        '#type' => 'password_confirm',
        '#required' => TRUE,
      ];

      $form['actions']['change_password'] = [
        '#type' => 'submit',
        '#value' => $this->t('Change password'),
        '#attributes' => [
          'class' => [
            'button',
            'button--primary',
          ],
        ],
      ];

      $form['uid'] = [
        '#type' => 'hidden',
        '#value' => $uid,
      ];
    }
    else {
      $message = $this->t('Unable to load the user details.');
      $this->messenger->addMessage($message, 'warning');
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\user\UserInterface $user */
    $user = $this->entityTypeManager->getStorage('user')->load($form_state->getValue('uid'));
    $user->setPassword($form_state->getValue('password'));
    $user->save();

    $form_state->setRedirectUrl(Url::fromUserInput('/admin/people'));

    // Let the activator know the user has been activated.
    $message = $this->t('The password for the user has been successfully updated.');
    $this->messengerService->addMessage($message, 'status');
  }

}
